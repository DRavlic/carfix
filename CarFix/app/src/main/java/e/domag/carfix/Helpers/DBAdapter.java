package e.domag.carfix.Helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DBAdapter {

    static final String DATABASE_NAME = "MyDB";
    static final String DATABASE_TABLE = "user";
    static final int DATABASE_VERSION = 2;

    static final String KEY_EMAIL = "email";
    static final String KEY_PHONE = "phone";
    static final String KEY_TOKEN = "token";
    static final String KEY_ROLE = "role";
    static final String TAG = "DBAdapter";

    static final String DATABASE_CREATE = "create table " + DATABASE_TABLE + " (email text primary key, phone text not null, token text not null, role text not null );";


    final Context context;

    DBHelper DBHelper;
    SQLiteDatabase db;

    public DBAdapter(Context ctx)
    {
        this.context = ctx;
        DBHelper = new DBHelper(context);
    }

    private static class DBHelper extends SQLiteOpenHelper
    {
        DBHelper(Context context)
        {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db)
        {
            try {
                db.execSQL(DATABASE_CREATE);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            Log.w(TAG, "Upgrading db from" + oldVersion + "to"
                    + newVersion );
            db.execSQL("DROP TABLE IF EXISTS user");
            onCreate(db);
        }
    }

    //---opens the database---
    public DBAdapter open() throws SQLException
    {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    //---closes the database---
    public void close()
    {
        DBHelper.close();
    }

    //---insert a contact into the database---
    public long insertUser(String email, String phone, String token, String role )
    {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_EMAIL, email);
        initialValues.put(KEY_PHONE, phone);
        initialValues.put(KEY_TOKEN, token);
        initialValues.put(KEY_ROLE, role);
        return db.insert(DATABASE_TABLE, null, initialValues);
    }

    //---deletes a particular contact---
    public boolean deleteUser()
    {
        return db.delete(DATABASE_TABLE, "1", null) > 0;
    }

    //---retrieves a particular contact---
    public Cursor getUser( ) throws SQLException
    {
        Cursor mCursor =
                db.query(true, DATABASE_TABLE, new String[] {KEY_EMAIL, KEY_PHONE, KEY_TOKEN, KEY_ROLE}, "1", null,
                        null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public Integer getcount( ) throws SQLException
    {
        Cursor mCursor =
                db.query(true, DATABASE_TABLE, new String[] {KEY_EMAIL, KEY_PHONE, KEY_TOKEN, KEY_ROLE}, "1", null,
                        null, null, null, null);
        if (mCursor != null) {
            mCursor.getCount();
        }
        return mCursor.getCount();
    }

    //---updates a contact---
    public boolean updateContact(String email, String password, String token, String role)
    {
        ContentValues args = new ContentValues();
        args.put(KEY_EMAIL, email);
        args.put(KEY_PHONE, password);
        args.put(KEY_TOKEN, token);
        args.put(KEY_ROLE, role);
        return db.update(DATABASE_TABLE, args, "1", null) > 0;
    }

}
