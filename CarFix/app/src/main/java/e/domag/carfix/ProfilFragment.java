package e.domag.carfix;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import e.domag.carfix.Helpers.Connect;
import e.domag.carfix.Helpers.Login;

public class ProfilFragment extends Fragment
{
    public View v = getView();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.profil_tab, container, false);

        Button povijestPopravakaButton = view.findViewById(R.id.povijestPopravaka_Button);
        povijestPopravakaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PovijestPopravakaActivity.class);
                getActivity().startActivity(intent);
            }
        });

        ProfilFragment.MainRequest r = new ProfilFragment.MainRequest();
        if(Login.getRole(getContext()).trim().equals("Driver"))
            r.execute("user/details");
        else
            r.execute("servicer/details");

        return view;
    }

    private class MainRequest extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {

            Map<String, String> array = new HashMap<>();

            array.put( "token", Login.getToken(getContext()) );

            Connect c = new Connect();
            return c.getData(urls[0], array );
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject object = new JSONObject( result );

                TextView name = getView().findViewById(R.id.ime_i_prezime_Profil);
                TextView phone = getView().findViewById(R.id.telMob_Profil);
                TextView eMail = getView().findViewById(R.id.eMail_Profil);

                name.setText(object.getString( "name" ));
                phone.setText(object.getString( "phone" ));
                eMail.setText(object.getString( "email" ));

            } catch (JSONException e1) {
                Toast.makeText(getContext(), "Error2" , Toast.LENGTH_LONG).show();
                e1.printStackTrace();
            }
        }
    }
}
