package e.domag.carfix;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import e.domag.carfix.Helpers.Connect;
import e.domag.carfix.Helpers.Login;

public class PronadiActivity extends FragmentActivity
        implements OnMapReadyCallback {

    private static final int MY_REQUEST_INT = 177;
    private GoogleMap map;

    public JSONArray mapJson;
    public String odabraniIdAutomehanicara;

    public ArrayList<LatLng> lokacijeAutomehanicara = new ArrayList<>();
    public ArrayList<String> imenaAutomehanicara = new ArrayList<>();
    public ArrayList<String> brojeviAutomehanicara = new ArrayList<>();
    public ArrayList<String> ideviAutomehanicara = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pronadi);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        PronadiActivity.MapRequest r = new PronadiActivity.MapRequest();
        r.execute("servicer/all");
    }

    private class MapRequest extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {

            Map<String, String> array = new HashMap<String, String>();

            array.put( "token", Login.getToken(getBaseContext()) );

            Connect c = new Connect();
            return c.getData(urls[0], array );
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                mapJson = new JSONArray(result);
                JSONObject mapJsonObject = new JSONObject();
                for(int i=0; i<mapJson.length(); i++)
                {
                    mapJsonObject = mapJson.getJSONObject(i);

                    imenaAutomehanicara.add(mapJsonObject.getString("name"));
                    brojeviAutomehanicara.add(mapJsonObject.getString("phone"));
                    double xLocation = Double.parseDouble(mapJsonObject.getString("map_x"));
                    double yLocation = Double.parseDouble(mapJsonObject.getString("map_y"));

                    lokacijeAutomehanicara.add(new LatLng(xLocation, yLocation));
                    ideviAutomehanicara.add(mapJsonObject.getString("id"));
                }

                LatLng zagrebLokacija = new LatLng(45.828322, 15.985239);

                if (ActivityCompat.checkSelfPermission(PronadiActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(PronadiActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    if(Build.VERSION.SDK_INT > Build.VERSION_CODES.M)
                    {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, MY_REQUEST_INT);
                    }

                    return;
                }
                else{
                    map.setMyLocationEnabled(true);
                }

                for (int i=0; i < lokacijeAutomehanicara.size(); i++)
                {
                    map.addMarker
                    (
                        new MarkerOptions()
                            .position(lokacijeAutomehanicara.get(i))
                            .title(ideviAutomehanicara.get(i) + ". " + imenaAutomehanicara.get(i))
                            .snippet(brojeviAutomehanicara.get(i))
                    );
                }
                map.moveCamera(CameraUpdateFactory.newLatLng(zagrebLokacija));

                map.moveCamera(CameraUpdateFactory.newLatLngZoom(zagrebLokacija,15));
                map.animateCamera(CameraUpdateFactory.zoomIn());
                map.animateCamera(CameraUpdateFactory.zoomTo(12), 2000, null);

                map.setOnInfoWindowLongClickListener(new GoogleMap.OnInfoWindowLongClickListener() {
                    @Override
                    public void onInfoWindowLongClick(Marker marker) {
                        Log.d("debug22", "tu sam");
                        if(Login.getRole(getBaseContext()).trim().equals("Driver")) {
                            odabraniIdAutomehanicara = marker.getTitle().substring(0, marker.getTitle().indexOf("."));

                            PronadiActivity.SendPopravakRequest r = new PronadiActivity.SendPopravakRequest();
                            r.execute("services/new");
                        }
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class SendPopravakRequest extends AsyncTask<String, Void, String> {


        protected String doInBackground(String... urls) {
            Map<String, String> array = new HashMap<String, String>();

            array.put("token", Login.getToken(getBaseContext()));
            array.put("servicer_id", odabraniIdAutomehanicara);

            Connect c = new Connect();
            return c.getData(urls[0], array);
        }

        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(PronadiActivity.this, "You made a deal!", Toast.LENGTH_LONG).show();

            PronadiActivity.this.finish();
        }
    }
}
