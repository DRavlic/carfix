package e.domag.carfix.Helpers;

import android.content.Context;
import android.database.Cursor;

public class Login {

    public static boolean login( String token, String email, String role, String phone, Context ctx ){

        DBAdapter db = new DBAdapter( ctx );
        db.open();
        db.deleteUser();
        db.close();

        db.open();
        long id = db.insertUser(email, phone, token, role );
        db.close();

        return true;
    }

    public static boolean logout( Context ctx ){
        DBAdapter db = new DBAdapter( ctx );
        db.open();
        boolean id = db.deleteUser();
        db.close();
        return true;
    }

    public static boolean isLoggedIn( Context ctx ){
        DBAdapter db = new DBAdapter( ctx );
        db.open();
        Integer c = db.getcount();
        db.close();
        if( c > 0 ) return true;
        return false;
    }

    public static String getRole( Context ctx ){
        DBAdapter db = new DBAdapter( ctx );
        db.open();
        Cursor user = db.getUser();
        db.close();
        return user.getString( 3 );
    }

    public static String getToken( Context ctx ){
        DBAdapter db = new DBAdapter( ctx );
        db.open();
        Cursor user = db.getUser();
        db.close();
        return user.getString( 2 );
    }
}
