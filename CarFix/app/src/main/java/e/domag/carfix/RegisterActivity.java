package e.domag.carfix;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import e.domag.carfix.Helpers.Connect;
import e.domag.carfix.Helpers.Login;

/**
 * A login screen that offers login via email/password.
 */
public class RegisterActivity extends AppCompatActivity {

    public static final String register_url = "register";

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                TextInputEditText address = findViewById(R.id.adress_Register);
                String x = data.getStringExtra("lokacijaLat");
                String y = data.getStringExtra("lokacijaLng");

                address.setText(x + ", " + y);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        /* dropdown */
        Spinner dropdown = findViewById(R.id.spinner1);
        String[] items = new String[]{"Driver", "Servicer"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);

        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            TextInputEditText adress = findViewById(R.id.adress_Register);
            Button mapButton = findViewById(R.id.map_Register);

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 1)
                {
                    adress.setHint("Address");
                    adress.setVisibility(View.VISIBLE);
                    mapButton.setVisibility(View.VISIBLE);
                }
                else
                {
                    adress.setHint("");
                    adress.setVisibility(View.INVISIBLE);
                    mapButton.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        Button mapButton = findViewById(R.id.map_Register);
        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, OdrediLokacijuActivity.class);
                startActivityForResult(intent, 1);
            }
        });
    }


    public void register(View view) {
        TextInputEditText address = findViewById(R.id.adress_Register);
        Spinner dropdown = findViewById(R.id.spinner1);

        if(dropdown.getSelectedItem().toString() == "Servicer" && address.getText().toString().isEmpty()){
            Toast.makeText(RegisterActivity.this, "Moras odabrati lokaciju!", Toast.LENGTH_SHORT).show();
            return;
        }

        RegisterRequest r = new RegisterRequest();
        r.execute( register_url );
    }


    private class RegisterRequest extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {

            Map<String, String> array = new HashMap<String, String>();

            EditText editText_name = findViewById(R.id.name);
            array.put( "name", editText_name.getText().toString() );
            EditText editText_email = findViewById(R.id.email_Register);
            array.put( "email", editText_email.getText().toString() );
            EditText phoneNumber = findViewById(R.id.phoneNumber_Register);
            array.put( "phone", phoneNumber.getText().toString() );
            EditText editText_password = findViewById(R.id.password_Register);
            array.put( "password", editText_password.getText().toString() );
            EditText editText_c_password = findViewById(R.id.c_password_Register);
            array.put( "c_password", editText_c_password.getText().toString() );
            Spinner dropdown = findViewById(R.id.spinner1);
            array.put( "role", dropdown.getSelectedItem().toString() );

            if(array.get("role") == "Servicer")
            {
                TextInputEditText address = findViewById(R.id.adress_Register);
                array.put( "address", address.getText().toString() );
            }
                Connect c = new Connect();
                return c.getData(urls[0], array );
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject object = new JSONObject( result );
                String value = object.getString("error");

                Toast.makeText(getBaseContext(), value, Toast.LENGTH_LONG).show();

            } catch (JSONException e) {
                e.printStackTrace();
                try {
                    JSONObject object = new JSONObject( result );
                    JSONArray keys = object.names ();

                    if
                    (
                        Login.login
                        (
                            object.getString( keys.getString(4) ), // token
                            object.getString( keys.getString(2) ), // email
                            object.getString( keys.getString(8) ), // role
                            object.getString( keys.getString(3) ), // phone
                            getBaseContext()
                        )
                    ){
                        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                        startActivity(intent);
                        setResult(RESULT_OK, null);
                        finish();
                    } else {
                        Toast.makeText(getBaseContext(), "Error1" , Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e1) {
                    Toast.makeText(getBaseContext(), "Error2" , Toast.LENGTH_LONG).show();
                    Toast.makeText(getBaseContext(), result , Toast.LENGTH_LONG).show();
                    e1.printStackTrace();
                }
            }
        }
    }
}