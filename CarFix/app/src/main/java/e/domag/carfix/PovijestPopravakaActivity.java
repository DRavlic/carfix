package e.domag.carfix;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import e.domag.carfix.Helpers.Connect;
import e.domag.carfix.Helpers.Login;

class Datum
{
    public int dan = 0;
    public int mjesec = 0;
    public int godina = 0;

    public Datum(int dan, int mjesec, int godina)
    {
        this.dan = dan;
        this.mjesec = mjesec;
        this.godina = godina;
    }

    public boolean seDogodioPrije(Datum date)
    {
        if(godina < date.godina)
            return true;
        else if(godina > date.godina)
            return false;

        if(mjesec < date.mjesec)
            return true;
        else if(mjesec > date.mjesec)
            return false;

        if(dan < date.dan)
            return true;
        else if(dan > date.dan)
            return false;

        return false;

    }

    @Override
    public String toString() {
        return String.valueOf(dan) + "/" + String.valueOf(mjesec) + "/" + String.valueOf(godina);
    }
}

public class PovijestPopravakaActivity extends AppCompatActivity {


    public JSONArray historyJson;

    private ArrayList<String> imenaDrugihOsoba = new ArrayList<>();
    private ArrayList<Datum> datumiPopravaka = new ArrayList<>();


    private void SortirajPolja()
    {
        for(int i=0; i < datumiPopravaka.size(); i++)
        {
            for(int j = 1; j < datumiPopravaka.size() - i; j++)
            {
                if(datumiPopravaka.get(j-1).seDogodioPrije(datumiPopravaka.get(j)))
                {
                    String temp;
                    // zamjena imena
                    temp = imenaDrugihOsoba.get(j-1);
                    imenaDrugihOsoba.set(j-1, imenaDrugihOsoba.get(j));
                    imenaDrugihOsoba.set(j, temp);

                    // zamjena datuma
                    Datum tempDate = datumiPopravaka.get(j-1);
                    datumiPopravaka.set(j-1, datumiPopravaka.get(j));
                    datumiPopravaka.set(j, tempDate);
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_povijest_popravaka);

        PovijestPopravakaActivity.HistoryRequest r = new PovijestPopravakaActivity.HistoryRequest();
        if(Login.getRole(getBaseContext()).trim().equals("Driver"))
            r.execute( "services/user" );
        else
            r.execute("services/servicer");
    }

    private class HistoryRequest extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {

            Map<String, String> array = new HashMap<String, String>();

            array.put( "token", Login.getToken(getBaseContext()) );

            Connect c = new Connect();
            return c.getData(urls[0], array );
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                historyJson = new JSONArray(result);
                JSONObject historyJsonObject = new JSONObject();
                for (int i = 0; i < historyJson.length(); i++) {
                    historyJsonObject = historyJson.getJSONObject(i);

                    if(Login.getRole(getBaseContext()).trim().equals("Driver")) {
                        String nesto = historyJsonObject.getString("servicer");
                        JSONObject servicerJsonObject = new JSONObject(nesto);

                        imenaDrugihOsoba.add(servicerJsonObject.getString("name"));
                    }
                    else
                    {
                        String nesto = historyJsonObject.getString("driver");
                        JSONObject servicerJsonObject = new JSONObject(nesto);

                        imenaDrugihOsoba.add(servicerJsonObject.getString("name"));
                    }

                    String datum = historyJsonObject.getString("created_at");
                    datumiPopravaka.add
                    (
                        new Datum
                        (
                            Integer.parseInt(datum.substring(8, 10)),
                            Integer.parseInt(datum.substring(5,7)),
                            Integer.parseInt(datum.substring(0,4))
                        )
                    );
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            // sortiramo to polje na osnovu datuma
            SortirajPolja();

            // dodavamo sve u linearLayout
            LinearLayout root = findViewById(R.id.povijestPopravaka);

            for(int i = 0; i < datumiPopravaka.size(); i++)
            {
                LinearLayout informacijeLayout = new LinearLayout(PovijestPopravakaActivity.this);
                informacijeLayout.setOrientation(LinearLayout.VERTICAL);
                informacijeLayout.setPadding(0, 30,0,50);

                TextView imePrezime = new TextView(PovijestPopravakaActivity.this);
                imePrezime.setText(imenaDrugihOsoba.get(i));
                imePrezime.setTextColor(Color.parseColor("#000000"));
                imePrezime.setTypeface(null, Typeface.BOLD);
                imePrezime.setTextSize(24);
                imePrezime.setGravity(Gravity.CENTER);

                TextView datum = new TextView(PovijestPopravakaActivity.this);
                datum.setText("Datum: " + datumiPopravaka.get(i).toString());
                datum.setTextSize(20);
                datum.setGravity(Gravity.CENTER);

                informacijeLayout.addView(imePrezime);
                informacijeLayout.addView(datum);

                root.addView(informacijeLayout);
            }
        }
    }
}
