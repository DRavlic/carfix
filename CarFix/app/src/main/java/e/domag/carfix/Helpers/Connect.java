package e.domag.carfix.Helpers;


import android.util.Log;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

public class Connect {

    private static final String urlString = "http://188.166.160.9/api/";

    private InputStream OpenHttpConnection(String s, String params) throws IOException
    {
        InputStream in = null;
        int response = -1;

        Log.d( "params" , params );

        URL url = new URL(urlString + s + "/" + params );
        URLConnection conn = url.openConnection();

        if (!(conn instanceof HttpURLConnection))
            throw new IOException("Not an HTTP connection");
        try{
            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.setDoInput( true );
            httpConn.setDoOutput( false );

            httpConn.connect();
            response = httpConn.getResponseCode();
            if (response == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();
            }
        }
        catch (Exception ex)
        {
            Log.d("Networking", ex.getLocalizedMessage());
            throw new IOException("Error connecting");
        }
        return in;
    }




    public String getData( String s, Map<String, String> array ){
        int BUFFER_SIZE = 2000;
        InputStream in = null;

        StringBuilder sbParams = new StringBuilder();
        int i = 0;

        JSONObject array2 = new JSONObject(array);
        String params = array2.toString();

        try {

            in = OpenHttpConnection( s, params );
        } catch (IOException e) {
            Log.d("Connect", e.getLocalizedMessage());
            return "";
        }



        InputStreamReader isr = new InputStreamReader(in);
        int charRead;
        String str = "";
        char[] inputBuffer = new char[BUFFER_SIZE];
        try {
            while ((charRead = isr.read(inputBuffer))>0) {
                //---convert the chars to a String---
                String readString = String.copyValueOf(inputBuffer, 0, charRead);
                str += readString;
                inputBuffer = new char[BUFFER_SIZE];
            }
            in.close();
        } catch (IOException e) {
            Log.d("NetworkingActivity", e.getLocalizedMessage());
            return "";
        }

        return str;
    }



}
